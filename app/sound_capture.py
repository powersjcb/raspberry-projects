"""
web server for demo'ing and testing animations
"""
import struct
import math
import audioop
import pyaudio


FORMAT = pyaudio.paInt16
RATE = 44100
INPUT_BLOCK_TIME = 0.05
INPUT_FRAMES_PER_BLOCK = int(RATE*INPUT_BLOCK_TIME)
SHORT_NORMALIZE = (1.0/32768.0)


def get_rms(block):

    # RMS amplitude is defined as the square root of the
    # mean over time of the square of the amplitude.
    # so we need to convert this string of bytes into
    # a string of 16-bit samples...

    # we will get one short out for each
    # two chars in the string.
    count = len(block)/2
    format_block = "%dh" % count
    shorts = struct.unpack(format_block, block)

    # iterate over the block.
    sum_squares = 0.0
    for sample in shorts:
        # sample is a signed short in +/- 32768.
        # normalize it to 1.0
        n = sample * SHORT_NORMALIZE
        sum_squares += n * n

    return math.sqrt(sum_squares / count)


def find_input_device(pa):
    device_index = None
    for i in range(pa.get_device_count()):
        devinfo = pa.get_device_info_by_index(i)
        print("Device %d: %s" % (i, devinfo["name"]))

        for keyword in ["mic", "input", "USB PnP"]:
            if keyword in devinfo["name"].lower():
                print("Found an input: device %d - %s" % (i, devinfo["name"]))
                device_index = i
                return device_index

    if device_index is None:
        print("No preferred input found; using default input device.")

    return device_index


class AudioStream(object):

    def __init__(self):
        self.pa = pyaudio.PyAudio()
        self.input_device_index = find_input_device(self.pa)
        self.stream = self.pa.open(format=FORMAT,
                                   channels=1,
                                   rate=RATE,
                                   input=True,
                                   frames_per_buffer=INPUT_FRAMES_PER_BLOCK)

    @property
    def rms(self) -> float:
        return audioop.rms(self.stream.read(INPUT_FRAMES_PER_BLOCK, exception_on_overflow=False), 2) / 50


if __name__ == '__main__':
    print(AudioStream().rms)


