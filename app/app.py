#!/usr/bin/env python
from typing import List

import numpy as np
import math
import random
import colorsys
import asyncio

import opc

from sound_capture import AudioStream


WAIT_TIMEOUT = 1 / 30
number_strips = 6
numLEDs = 60 * number_strips
REFRESH_RATE = 40  # Hz
AUDIO_SAMPLE_RATE = 10

SPARKLE_INTENSITY = (150, 150, 50)
SPARKLE_RATE = 20
SPARKLE_CHANCE = 0.01

COLOR_FADE_REFRESH_RATE = 20
COLOR_FADE_SPEED = 0.04
COLOR_LUMINANCE = 10

WAVING_SPEED = 0.5
ELECTRON_SPEED = 30


client = opc.Client('127.0.0.1:7890')

RED_OFFSET = math.pi/3
BLUE_OFFSET = math.pi*2/3
GREEN_OFFSET = math.pi*3/3


class Pixel(list):
    """
    rgb pixel as represented by opc
    """
    COLORS = {
        'red': (255, 0, 0),
        'blue': (0, 255, 0),
        'green': (0, 0, 255),
        'white': (255, 255, 255),
        'black': (0, 0, 0),
    }

    def __init__(self, r, g, b):
        super().__init__([r, g, b])
        self.r = r
        self.g = g
        self.b = b

    def __repr__(self):
        return 'r: %s g: %s b: %s' % (self.r, self.g, self.b)

    @property
    def hls(self):
        return colorsys.rgb_to_hls(self.r, self.g, self.b)

    @property
    def hue(self):
        return self.hls[0]

    @property
    def luminance(self):
        return self.hls[1]

    def lumen_copy(self, lumen):
        return Pixel(*colorsys.hls_to_rgb(self.hue, int(self.luminance*lumen), self.saturation))

    @property
    def saturation(self):
        return self.hls[2]

    @classmethod
    def oscillating_color(cls, time, brightness, speed=1):
        hue = (time * speed)
        saturation = 1.0
        return cls(*colorsys.hsv_to_rgb(hue, saturation, brightness))

    @classmethod
    def color(cls, color_name):
        return cls(*cls.COLORS[color_name])


class Pattern(object):

    def render(self, time):
        buffer_frame = round(time * self.refresh_rate) % len(self.pattern_buffer)
        return self.pattern_buffer[buffer_frame]

    def display(self, time) -> bool:
        return client.put_pixels(self.render(time))


class WavingRainbow(Pattern):

    def __init__(self, num_pixels: int, speed, rainbow_count, refresh_rate) -> None:
        self.rainbow_count = rainbow_count  # how many instances per set set of pixels
        self.num_pixels = num_pixels
        self.speed = speed  # cycles per second
        self.refresh_rate = refresh_rate

        frame_duration = float(1 / refresh_rate)
        self.pattern_buffer = [self.generate_pattern(t) for t in np.arange(0, speed * refresh_rate, frame_duration)]

    def generate_pattern(self, time: float) -> List[Pixel]:
        return [Pixel.oscillating_color(time*self.speed + i*self.num_pixels/360/60, COLOR_LUMINANCE)
                for i in range(self.num_pixels)]


class Electron(Pattern):

    def __init__(self, num_pixels, speed, refresh_rate, count=1, tail_length=10, color=(255, 255, 255), event_loop=None):
        self.loop = event_loop
        self.tail_length = tail_length
        self.num_pixels = num_pixels
        self.speed = speed
        self.count = count
        self.refresh_rate = refresh_rate
        self.color = color
        duration = speed * refresh_rate
        frame_duration = float(1 / refresh_rate)
        self.pattern_buffer = [self.generate_pattern(t) for t in np.arange(0, duration, frame_duration)]

    def generate_pattern(self, time, pixels=None):
        if not pixels:
            pixels = [Pixel.color('black')] * self.num_pixels
        position0 = int(time * self.speed * self.num_pixels) % self.num_pixels
        positions = [(position0 + int(i * numLEDs / self.count)) % self.num_pixels for i in range(0, self.count)]

        for p in positions:
            pixels[p] = Pixel(*self.color)

        for position in positions:
            for i in range(1, self.tail_length):
                pixels[position - i] = pixels[position].lumen_copy(0.05 * (self.tail_length - i))

        return pixels

    def render(self, time):
        if self.loop:
            self.generate_pattern(time, pixels=loop.pixels)
        return super(Electron, self).render(time)

async def render(loop):
    while True:
        client.put_pixels(loop.pixels)
        await asyncio.sleep(1 / REFRESH_RATE
)

async def sparkle(loop):
    while True:
        if 'sparkle' in loop.active_patterns:
            brighten = random.sample(range(0, numLEDs-1), int(numLEDs*SPARKLE_CHANCE))
            darken = random.sample(range(0, numLEDs-1), int(numLEDs*SPARKLE_CHANCE))
            for br in brighten:
                loop.pixels[br] = Pixel(*SPARKLE_INTENSITY)
            for dr in darken:
                loop.pixels[dr] = Pixel(0, 0, 0)
            await asyncio.sleep(1/SPARKLE_RATE)
        else:
            await asyncio.sleep(WAIT_TIMEOUT)


async def get_strobed(loop):
    while True:
        if 'strobe' in loop.active_patterns:
            STROBE_INTENSITY = (100, 100, 100)
            if loop.sound_rms > loop.sound_avg * 1.2:
                # print(loop.sound_rms, loop.sound_avg)
                loop.pixels = [Pixel(*STROBE_INTENSITY)] * numLEDs
            await asyncio.sleep(1/REFRESH_RATE)
        else:
            await asyncio.sleep(WAIT_TIMEOUT)


async def color_fade(loop):
    """main pattern"""
    while True:
        if 'color_fade' in loop.active_patterns:
            loop.pixels = [Pixel.oscillating_color(loop.time(), COLOR_LUMINANCE, speed=COLOR_FADE_SPEED)] * numLEDs
            await asyncio.sleep(1/COLOR_FADE_REFRESH_RATE)
        else:
            await asyncio.sleep(WAIT_TIMEOUT)


async def prerender_rainbow(loop):
    rainbow = WavingRainbow(num_pixels=numLEDs, speed=1.0, rainbow_count=1, refresh_rate=COLOR_FADE_REFRESH_RATE)
    while True:
        if 'prerender_rainbow' in loop.active_patterns:
            loop.pixels = rainbow.render(loop.time())
            await asyncio.sleep(1/REFRESH_RATE)
        else:
            await asyncio.sleep(WAIT_TIMEOUT)


async def electron(loop):
    electron = Electron(num_pixels=numLEDs, speed=1, refresh_rate=REFRESH_RATE,
                        count=1, color=(100, 100, 100), tail_length=10, event_loop=loop)
    while True:
        if 'electron' in loop.active_patterns:
            loop.pixels = electron.render(loop.time())
            await asyncio.sleep(1/REFRESH_RATE)
        else:
            await asyncio.sleep(WAIT_TIMEOUT)


async def black(loop):
    while True:
        for color in Pixel.COLORS:
            if color in loop.active_patterns:
                loop.pixels = [Pixel.color(color)] * numLEDs
                await asyncio.sleep(1/REFRESH_RATE)
                break
            await asyncio.sleep(WAIT_TIMEOUT)


async def audio_sample(loop):
    n = 1
    while True:
        loop.sound_rms = loop.audio.rms
        loop.sound_avg = (loop.sound_avg + loop.sound_rms * n) / (n + 1)
        await asyncio.sleep(1/REFRESH_RATE)


PATTERNS = [
    {'prerender_rainbow'},
    # {'prerender_rainbow', 'sparkle'},
    # {'sparkle'},
    # {'black', 'strobe'},
    # {'black', 'sparkle'},  # strobe
    # {'electron'},
    # {'prerender_rainbow', 'electron'},
]


async def pattern_controller(loop):
    while True:
        loop.active_patterns = random.choice(PATTERNS)
        await asyncio.sleep(16)


if __name__ == '__main__':
    audio = AudioStream()
    loop = asyncio.get_event_loop()
    loop.audio = audio
    loop.sound_rms = audio.rms
    loop.sound_avg = audio.rms
    loop.pixels = [Pixel.color('black')] * numLEDs
    loop.active_patterns = PATTERNS[0]
    loop.run_until_complete(asyncio.gather(
        render(loop),
        sparkle(loop),  # 0.1 is fucking insane looking
        electron(loop),
        black(loop),
        prerender_rainbow(loop),
        pattern_controller(loop),
        audio_sample(loop),
        get_strobed(loop),
    ))
