import processing.sound.AudioIn;
import codeanticode.syphon.*;

OPC opc;
PImage img;
SyphonClient client;


static int pixels_wide = 1280;
static int pixels_high = 720;
static int display_scale = 8;
static int numStrips = 8;

boolean gracefull_shutdown = false;


void setup() {
  opc = new OPC(this, "127.0.0.1", 7890);
  client = new SyphonClient(this);
  size(1280, 720, P3D);
  PJOGL.profile = 1;
  frameRate(60);
}


void draw() {
  background(0); 
  if (client.newFrame()) {
    // The first time getImage() is called with 
    // a null argument, it will initialize the PImage
    // object with the correct size. //<>//
    //img = client.getImage(img); // load the pixels array with the updated image info (slow)
    img = client.getImage(img, false); // does not load the pixels array (faster)    
  }
  if (img != null) {
    if (gracefull_shutdown != true) {
      image(img, 0, 0, width, height);
    } else {
      color(0, 0 ,0 );  // fade to black before shutdown
    }
    float grid_center_x = width / 2;
    float grid_center_y = height / 2;
    float spacing_x = width / 30;
    float spacing_y = height / 8;
    //ledStrip(int index, int count, float x middle, float y middle, float spacing, float angle, boolean reversed)
    float scale = 200.0; // overall-size / pixel spacing
    float spacing = height / scale;
    float strip_length = spacing * 30;
     //opc.ledStrip(i * 60, 60, width / 2, (spacing * i) - 1, spacing, 0, false);
     
     // 1. bottom horizontal of triangle 
     //float bottom_y = height / 2 + 0.289 * strip_length;
     //float bottom_x = width / 2;
     //float bottom_angle = 0;
     //opc.ledStrip(0 * 60, 60, bottom_x, bottom_y, spacing, bottom_angle, false);
     
     // 2. right side of triangle
     float right_y = height / 2 - strip_length * 0.14438;
     float right_x =  width/ 2 + strip_length * 0.25;
     float right_angle = - 2.0944; // 120 degrees
     opc.ledStrip(15, 15, right_x, right_y, spacing, right_angle, false);
     
     // 3. left side of triangle
     float left_y = height / 2 - strip_length * 0.14438;
     float left_x =  width/ 2 - strip_length * 0.25;
     float left_angle =  2.0944; // 120 degrees
     opc.ledStrip(30, 15, left_x, left_y, spacing, left_angle, false);

  }
}


void keyPressed() {
  if (key == ' ') {
    gracefull_shutdown = true;
  }
  if (gracefull_shutdown == true) {
    client.stop(); 
  } 
  System.out.println(frameRate);
}