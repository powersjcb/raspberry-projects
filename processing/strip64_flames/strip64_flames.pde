// install:
// controlP5
// oscP5

import processing.sound.AudioIn;
import processing.sound.Amplitude;
import processing.sound.FFT;
import oscP5.OscP5;
import oscP5.OscMessage;
import controlP5.ControlP5;


OscP5 oscP5;
ControlP5 cp5;

AudioIn in;
FFT fft;
OPC opc;
PImage im;


static int numStrips = 8;
static int pixelsPerStrip = 60;
static int MAX_BRIGHTNESS = 255;
static float MAX_HUE = 255;
static float MAX_SATURATION = 255;
static float VELOCITY_COEF_RANGE = 20;
static float STATIC_VELOCITY_RANGE = .5;

// mutable variables 
int brightness = MAX_BRIGHTNESS;
float hue = 0;
float var = 0;


static int fftBands = 256;
float[] spectrum = new float[fftBands];

void setup()
{
  oscP5 = new OscP5(this, 6666);
  
  cp5 = new ControlP5(this); 
  cp5.addSlider("brightness")
        .setRange(0, MAX_BRIGHTNESS)
        .setValue(MAX_BRIGHTNESS / 2)
        .setSize(100, 20);
  cp5.addSlider("hue")
        .setRange(0, MAX_HUE)
        .setValue(0)
        .setSize(100, 20);
  cp5.addSlider("saturation")
        .setRange(0, MAX_SATURATION)
        .setValue(MAX_SATURATION / 2)
        .setSize(100, 20);
  cp5.addSlider2D("velocity_control")
        .setMinMax(-VELOCITY_COEF_RANGE, -VELOCITY_COEF_RANGE, VELOCITY_COEF_RANGE, VELOCITY_COEF_RANGE)
        .setValue(0.5 * VELOCITY_COEF_RANGE, 0.1 * VELOCITY_COEF_RANGE)
        .setSize(height, height);

  cp5.addSlider("static_velocity")
        .setRange(0, STATIC_VELOCITY_RANGE)
        .setValue(0.01)
        .setSize(100, 20);
        
  // Audio setup
  in = new AudioIn(this, 0);
  fft = new FFT(this, fftBands);
  in.start();
  fft.input(in);
  
  size(880, 64);
  colorMode(HSB);

  // Load a sample image
  //gif-acide-01.gif gif-acide-02.gif
  //im = loadImage("gif-acide-01.gif");
  //im = loadImage("flames.jpeg");
  //im = loadImage("bubbles.jpg");
  im = loadImage("tetris.png");
  // Connect to the local instance of fcserver
  opc = new OPC(this, "127.0.0.1", 7890);
  
  float spacing = height / numStrips * 0.5;
  for (int i=0; i < numStrips; i++) {
    opc.ledStrip(i * 60, 60, width/2, (spacing * i), spacing, 0, false);
  }
  frameRate(60);
  //fft.logAverages(22, 60);
}

float x0 = 0;
float y0 = 0;
float t0 = 0;

void draw()
{
  fft.analyze(spectrum);
  
  float bass_volume = 0;
  for (int i=0; i < 150; i++) {
    bass_volume = bass_volume + spectrum[i];
  }

  // Scale the image so that it matches the width of the window
  int imHeight = im.height * width / im.width;
  // Scroll down slowly, and wrap around
  float static_vel = cp5.getController("static_velocity").getValue();
  float volume_coef_x = cp5.getController("velocity_control").getArrayValue()[0];
  float volume_coef_y = cp5.getController("velocity_control").getArrayValue()[1];
  System.out.println(volume_coef_x);
  float speed_x = static_vel + bass_volume * volume_coef_x;
  float speed_y = static_vel + bass_volume * volume_coef_y;
  float t = millis();
  float y = -((-y0 + (t - t0) * speed_y) % imHeight);
  float x = -((-x0 + (t - t0) * speed_x) % width);
  y0 = y;
  x0 = x;
  t0 = t;
  // Use two copies of the image, so it seems to repeat infinitely  
  image(im,           x, y,            width, imHeight); // center
  image(im,           x, y - imHeight, width, imHeight); // bottom center
  image(im,   width + x, y,            width, imHeight); // middle right
  image(im,   width + x, y - imHeight, width, imHeight); // bottom right
  image(im, - width + x, y - imHeight, width, imHeight); // bottom left
  image(im, - width + x, y,            width, imHeight); // middle left
  image(im,           x, y + imHeight, width, imHeight); // center top
  image(im, - width + x, y + imHeight, width, imHeight); // top left  
  image(im,   width + x, y + imHeight, width, imHeight); // top right  


  tint(cp5.getController("hue").getValue(),
       cp5.getController("saturation").getValue(), 
       cp5.getController("brightness").getValue());
}

void oscEvent(OscMessage theOscMessage) {
 
  // page 1
  if(theOscMessage.checkAddrPattern("/1/fader5")==true) {
    float value = theOscMessage.get(0).floatValue();
    float b = value * MAX_BRIGHTNESS;
    cp5.getController("brightness").setValue(b);
  }
  
  if(theOscMessage.checkAddrPattern("/1/fader1")==true) {
    float value = theOscMessage.get(0).floatValue();
    float b = value * MAX_HUE;
    cp5.getController("hue").setValue(b);
  }
  
  if(theOscMessage.checkAddrPattern("/1/fader2")==true) {
    float value = theOscMessage.get(0).floatValue();
    float b = value * MAX_SATURATION;
    cp5.getController("saturation").setValue(b);
  }
  
  //if(theOscMessage.checkAddrPattern("/1/fader3")==true) {
  //  float value = theOscMessage.get(0).floatValue();
  //  float b = value * VELOCITY_COEF_RANGE;
  //  cp5.getController("velocity_coef").setValue(b);
  //}
  
if(theOscMessage.checkAddrPattern("/1/fader4")==true) {
    float value = theOscMessage.get(0).floatValue();
    float b = value * STATIC_VELOCITY_RANGE;
    cp5.getController("static_velocity").setValue(b);
  }
}