

deploy:
	$(MAKE) -C deployment deploy

patch: clean
	$(MAKE) -C deployment patch

app_venv:
	virtualenv --python=python3.6 venv

build:
	. venv/bin/activate && pip install -r app/requirements.txt

clean:
	find . -name "*pyc" -delete
	rm -rf app/__pycache__/

fcserver:
	./bin/fcserver-osx bin/fcserver_config.json

processing %:
	pkill java || true
	rm -rf /tmp/processing
	mkdir /tmp/processing
	$(HOME)/processing-java --output=/tmp/processing/ --force --sketch=$(shell pwd)/processing/$(@) --run
