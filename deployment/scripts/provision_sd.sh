#!/usr/bin/env bash
set -x

IMG_PATH="/Users/jpowers/projects/raspberry-projects/2016-05-27-raspbian-jessie-lite.img"

# ENSURE THIS IS CORRECT OR BAD THINGS HAPPEN!
DISK_PATH="/dev/disk2"
NEW_DISK_PATH="/dev/rdisk2"

diskutil list

# format the disk as FAT32
sudo diskutil eraseDisk FAT32 RAPBERRYSD MBRFormat ${DISK_PATH}

diskutil unmountDisk ${DISK_PATH}


sudo dd bs=1m if=${IMG_PATH} of=${NEW_DISK_PATH}
sudo diskutil eject ${NEW_DISK_PATH}
